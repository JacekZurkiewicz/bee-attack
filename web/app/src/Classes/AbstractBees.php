<?php

namespace BeesAttack\Classes;

use Noodlehaus\Config;

class AbstractBees
{

    private $config;
    private $hive = array();

    /**
     * AbstractBees constructor.
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->createBeesHive();
    }

    /**
     *
     * Create Hive with Bees
     *
     * @return array
     */
    private function createBeesHive()
    {
        $bees = $this->createBeesArray($this->getBeeList());
        $amountOfBees = $this->getAmountOfBees();

        foreach ($bees as $key => $value){
            // next step
        }

        return $this->hive;
    }

    /**
     *
     * Create bees
     *
     * @param $beesList
     * @return array
     */
    private function createBeesArray($beesList)
    {
     $bees = array();

        foreach ($beesList as $bee){
            $bees[$bee] = array(
                'name'  => $this->config->get($bee.'name'),
                'lifespan'  => $this->config->get($bee.'lifespan'),
                'hit'  => $this->config->get($bee.'hit'),
            );
        }
        return $bees;
    }

    /**
     *
     * Get bee List
     *
     * @return mixed
     */
    private function getBeeList()
    {
       return pre_sprit(",",$this->config->get('beesList'));
    }

    /**
     *
     * Get amount of bees
     *
     * @return mixed
     */
    private function getAmountOfBees()
    {
        return pre_sprit(",",$this->config->get('amountOfBees'));
    }
}