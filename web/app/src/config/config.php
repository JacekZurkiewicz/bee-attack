<?php

namespace BeesAttack;

use Noodlehaus\Config;
use Noodlehaus\Parser\Yaml;

$settingsYaml = <<<FOOBAR
typeBee:
     beeOne: 'queen'
     beeTwo: 'worker'
     beeThree: 'drone
lifespan:
    queen: 100
    worker: 75
    drone: 50
hit:
    queen: 8
    worker: 10
    drone: 12
hit:
    queen: 8
    worker: 10
    drone: 12    
quantity:
    queen: 1
    worker: 5
    drone: 8

FOOBAR;

$conf = new Config($settingsYaml, new Yaml, true);