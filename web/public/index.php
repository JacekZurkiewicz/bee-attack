<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Jacek Żurkiewicz">
    <title>Bees Attack</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</head>
<body class="container">
<div class="checkbox mb-3">
    <h1 class="mt-1 text-center">
        Bees Attack
    </h1>
</div>
<div id="hive">
    <p id="error"><strong>END OFF GAME <br>YOU WIN</strong></p>
    <!-- Table  -->
    <table class="table table-bordered">
        <!-- Table body -->
        <tbody>
        <tr>
            <th scope="row">
                <div class="custom-control">
                    <p>Queen</p>
                </div>
            </th>
            <td>100%</td>
        </tr>
        <tr>
            <th scope="row">
                <div class="custom-control">
                    <p>Worker</p>
                </div>
            </th>
            <td>100%</td>
            <td>100%</td>
            <td>100%</td>
            <td>100%</td>
            <td>100%</td>
        </tr>
        <tr>
            <th scope="row">
                <div class="custom-control">
                    <p>Dron</p>
                </div>
            </th>
            <td>100%</td>
            <td>100%</td>
            <td>100%</td>
            <td>100%</td>
            <td>100%</td>
            <td>100%</td>
            <td>100%</td>
            <td>100%</td>
        </tr>
        </tbody>
        <!-- Table body -->
    </table>
    <!-- Table  -->
</div>
<div class="col-12">
    <button class="btn btn-lg btn-primary">HIT BEE</button>
</div>

<script>

    $("button").click(function(){
        alert("The paragraph was clicked.");
        $.ajax({url: "demo_test.txt",
         success: function(result){
                $("#hive").html(result);
            },
        error: function (result) {
                $("#error").html(result);
        }
        });
    });
</script>
</body>
</html>